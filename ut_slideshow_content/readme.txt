UT Drupal Kit Add-On - Slideshow

To make this work:
-Copy the included jquery.cycle folder to sites/all/libraries
-Make sure to have Views Slideshow contrib module already installed

There is an included Sample Slideshow which is by default available
to Page Builder as a block. It will display the nodes on the site 
and the node number as a scrolling slideshow. 